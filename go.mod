module acm-ui

go 1.16

require (
	github.com/aws/aws-sdk-go-v2 v1.2.0 // indirect
	github.com/aws/aws-sdk-go-v2/config v1.1.1
	github.com/aws/aws-sdk-go-v2/service/acm v1.1.1
	github.com/mitchellh/hashstructure/v2 v2.0.1
)
