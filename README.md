# ACMUI

This repository contains an alternative improved UI for Amazon's ACM.

## Usage

This software is meant to be used entirely within docker. It currently isn't hosted in any docker repos, though.

To build from the root of the repository (with docker tag `acmui:latest`):

```bash
docker build . -t acmui:latest
```

To run (accessible on your host machine at `localhost:80`):

```bash
docker run -v $HOME/.aws:/.aws/ -p 80:80 -e ACMUI_REGIONS="ca-central-1,us-east-1,eu-west-1" -e AWS_CONFIG_FILE="/.aws/config" acmui:latest
```

If you have multiple AWS profiles configured, you may need to configure the `AWS_PROFILE` environment variable in the docker container.

## Motivation

Amazon's ACM Web UI is horrible. It had several problems, including:

* It is unsearchable
* It only shows certificates from a single region, which while somewhat standard for region-locked services, is inconvenient for my use case
* The UI is incredibly slow
* Loading tags for a certificate frequently breaks

This project was also a great opportunity to improve my full-stack development skills.

## Is it any good?

Yes.

## Maintainers

Cooper Worobetz
