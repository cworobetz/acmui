package main

import (
	"log"
	"time"
)

func timeTrack(start time.Time, prompt string) {
	elapsed := time.Since(start)
	log.Printf("%s %s", prompt, elapsed)
}
