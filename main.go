package main

import (
	"encoding/csv"
	"log"
	"net/http"
	"os"
	"strings"
)

var allCerts AWSCertificateList // Cache that we serve to clients / update with certificates
var summaryHashCache = make(map[string]uint64)
var regions []string // Config containing which regions we will fetch certs from

func main() {

	parseOptions()

	// File server handles static content
	fs := http.FileServer(http.Dir("/srv/static"))
	http.Handle("/", fs)

	// Register routes
	http.HandleFunc("/api/getcerts", getCerts)
	http.HandleFunc("/api/updatecerts", updateCerts)

	// Concurrently fetches all certificates in a background goroutine. Appends results to allCerts
	log.Println("Fetching all certificates and tags from AWS")
	go fetchAllCerts(regions)

	log.Println("Listening on :80...")
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func parseOptions() {
	r := csv.NewReader(strings.NewReader(os.Getenv("ACMUI_REGIONS")))
	var err error
	regions, err = r.Read()
	if err != nil {
		log.Panicf("Error parsing environment variables: %v", err)
	}
}
