// Globals
var table;
var certs; // For easier debugging. To be removed

// Filters
var fieldEl = document.getElementById("filter-field");
var valueEl = document.getElementById("filter-value");

// Called when user clicks Update Certificates
function updatecerts() {
    table.setData("/api/updatecerts")
}

// Sweet slide up notification
$("#alert-success").hide();

// Called whenever text is put into the search box
function updateFilter() {
    if (fieldEl.value) {
        table.setFilter(fieldEl.value, "like", valueEl.value);
    }
}

// Helps with pretty printing JSON
if (!library) {
    var library = {};
}

library.json = {
    replacer: function (match, pIndent, pKey, pVal, pEnd) {
        var key = '<span class=json-key>';
        var val = '<span class=json-value>';
        var str = '<span class=json-string>';
        var r = pIndent || '';
        if (pKey)
            r = r + key + pKey.replace(/[": ]/g, '') + '</span>: ';
        if (pVal)
            r = r + (pVal[0] == '"' ? str : val) + pVal + '</span>';
        return r + (pEnd || '');
    },
    prettyPrint: function (obj) {
        var jsonLine = /^( *)("[\w]+": )?("[^"]*"|[\w.+-]*)?([,[{])?$/mg;
        return JSON.stringify(obj, null, 3)
            .replace(/&/g, '&amp;').replace(/\\"/g, '&quot;')
            .replace(/</g, '&lt;').replace(/>/g, '&gt;')
            .replace(jsonLine, library.json.replacer);
    }
};

//Update filters on value change
document.getElementById("filter-field").addEventListener("change", updateFilter);
document.getElementById("filter-value").addEventListener("keyup", updateFilter);

//Clear filters on "Clear Filters" button click
document.getElementById("filter-clear").addEventListener("click", function () {
    fieldEl.value = "";
    valueEl.value = "";

    table.clearFilter();
});

function reformatData(data) {
    data.ACMCertificates.forEach(function(row) {
        cache = row.ACMTags.Tags
        delete row.ACMTags.Tags
        row.ACMTags.Tags = {}
        cache.forEach(function(tag) {
            row["ACMTags"]["Tags"][tag.Key] = tag.Value
        });
    });
}

// Create the table settings
table = new Tabulator("#table", {
    layout: "fitColumns",
    movableColumns: true,
    rowClick: function (e, row) {
        $('#exampleModal').modal('show');
        $('#delete-certificate').attr("Value", row._row.data.ACMCertificate.Certificate.CertificateArn);
        $('#modal-text').html(library.json.prettyPrint(row._row.data));
    },
    columns: [
        { title: "CertificateArn", field: "ACMCertificate.Certificate.CertificateArn", sorter: "string" },
        { title: "DomainName", field: "ACMCertificate.Certificate.DomainName", sorter: "string" },
        { title: "NotAfter", field: "ACMCertificate.Certificate.NotAfter", sorter: "string" },
        { title: "InUseBy", field: "ACMCertificate.Certificate.InUseBy", sorter: "string" },
        { title: "ImportedAt", field: "ACMCertificate.Certificate.ImportedAt", sorter: "string" },
    ],
    ajaxResponse: function (url, params, response) {
        $("#alert-success").html("<strong>Success! Fetched " + response.ACMCertificates.length + " certs");
        $("#alert-success").fadeTo(2000, 500).slideUp(500, function() {
          $("#alert-success").slideUp(500);
        });
        reformatData(response);
        certs = response;
        return response["ACMCertificates"];
    },
    dataFiltered: function(filters, rows) {
        $("#search-row-count").text(rows.length + " Rows found");
    },
    progressiveRender: true,
    pagination: "local",
    paginationSize: 15,
});

// Fetch the data
table.setData("/api/getcerts")

function deletecertificate(button) {
    confirm("Really delete " + button.value + "?")
}