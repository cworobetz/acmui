FROM golang:alpine AS build-env

WORKDIR $GOPATH/src/acm-ui

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 go build -o /go/bin/acm-ui

FROM scratch
COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build-env /go/bin/acm-ui /srv/acm-ui
COPY ./static /srv/static

EXPOSE 80

WORKDIR /srv
ENTRYPOINT ["./acm-ui"]
