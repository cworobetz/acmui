package main

import (
	"encoding/json"
	"net/http"

	"log"
)

// updateCerts updates the local cache and returns a JSON response containing detailed information about all certificates
func updateCerts(w http.ResponseWriter, r *http.Request) {
	log.Printf("Certificate cache update requested by %s", r.RemoteAddr)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	fetchAllCerts(regions)
	resp, err := json.Marshal(allCerts)
	if err != nil {
		log.Printf("Error marshalling json data: %v\n", err)
	}
	log.Printf("Serving updated certificate cache to %s", r.RemoteAddr)
	w.Write(resp)
}

// getCerts sends a JSON response containing detailed information about all certificates
func getCerts(w http.ResponseWriter, r *http.Request) {
	log.Printf("Serving cached certs to %s", r.RemoteAddr)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	resp, err := json.Marshal(allCerts)
	if err != nil {
		log.Printf("Error marshalling json data: %v\n", err)
	}
	w.Write(resp)
}
