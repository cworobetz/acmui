package main

import (
	"context"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/acm"
	"github.com/mitchellh/hashstructure/v2"
)

type ACMCertificate struct {
	ACMCertificate *acm.DescribeCertificateOutput
	ACMTags        *acm.ListTagsForCertificateOutput
}
type AWSCertificateList struct {
	ACMCertificates []ACMCertificate
	LastUpdate      int64
}

// fetchAllCerts is responsible for calling fetchDescribeCerts for all configured regions
func fetchAllCerts(regions []string) {

	defer timeTrack(time.Now(), "Fetching all certificates and tags took")

	// regionWg allows us to detect when a goroutine has finished. In this case, when a region is done fetching all certs.
	var wg sync.WaitGroup

	for _, region := range regions {
		wg.Add(1)
		go fetchDescribeCerts(region, &wg)
	}

	wg.Wait()
	allCerts.LastUpdate = time.Now().Unix()
	log.Printf("Found %d certificates", len(allCerts.ACMCertificates))
}

// fetchDescribeCerts is responsible for fetching a list of all certificatesummaries in a given region,
// and passing each certificate ARN to fetchDescribeCert
func fetchDescribeCerts(region string, wg *sync.WaitGroup) {

	defer wg.Done()

	// Create a configuration for our client to use. This config specifies the region we want
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion(region))
	if err != nil {
		log.Fatalf("Unable to load SDK config, %v", err)
	}

	// Client we use for queries to this region
	client := acm.NewFromConfig(cfg)

	// Fetch a list of all certificates in this region
	maxItems := int32(1000)
	resp, err := client.ListCertificates(context.TODO(), &acm.ListCertificatesInput{MaxItems: &maxItems})
	if err != nil {
		log.Panic(err)
	}

	// To reduce unnecessary API calls, we check to see if anything has changed since last time
	hash, err := hashstructure.Hash(resp, hashstructure.FormatV2, nil)
	if summaryHashCache[region] != hash {

		summaryHashCache[region] = hash

		// Invalidate the cached certificates in that region since the hash doesn't match.
		for i, cert := range allCerts.ACMCertificates {
			if strings.Contains(*cert.ACMCertificate.Certificate.CertificateArn, region) {
				allCerts.ACMCertificates[i] = allCerts.ACMCertificates[len(allCerts.ACMCertificates)-1]
			}
		}

		// For every cert in this region, make a goroutine to fetch detailed information and tags
		for _, respCertSummary := range resp.CertificateSummaryList {
			certDetailsChan := make(chan *acm.DescribeCertificateOutput)
			certTagsChan := make(chan *acm.ListTagsForCertificateOutput)
			wg.Add(2)
			go fetchDescribeCertificate(client, &acm.DescribeCertificateInput{CertificateArn: respCertSummary.CertificateArn}, wg, certDetailsChan)
			go fetchListTagsForCertificate(client, &acm.ListTagsForCertificateInput{CertificateArn: respCertSummary.CertificateArn}, wg, certTagsChan)
			allCerts.ACMCertificates = append(allCerts.ACMCertificates, ACMCertificate{ACMCertificate: <-certDetailsChan, ACMTags: <-certTagsChan})
		}

	} else {
		log.Printf("API response for region %s matches cached API response. No changes detected.\n", region)
	}

}

// fetchDescribeCert is responsible for fetching information about a specific cert in a specific region, and appending that result to global variable allCerts
func fetchDescribeCertificate(client *acm.Client, params *acm.DescribeCertificateInput, wg *sync.WaitGroup, certDetailsChan chan *acm.DescribeCertificateOutput) {

	defer wg.Done()

	cert, err := client.DescribeCertificate(context.TODO(), params)
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Found certificate ARN %s", *cert.Certificate.CertificateArn)
	certDetailsChan <- cert
}

// fetchListTagsForCertificate is responsible for fetching a given certificate's tag
func fetchListTagsForCertificate(client *acm.Client, params *acm.ListTagsForCertificateInput, wg *sync.WaitGroup, certTagsChan chan *acm.ListTagsForCertificateOutput) {

	defer wg.Done()

	tags, err := client.ListTagsForCertificate(context.TODO(), params)
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Found tags for certificate ARN %s", *params.CertificateArn)
	certTagsChan <- tags
}
